#!/bin/sh
if [ "${ASPNETCORE_ENVIRONMENT+set}" = set ] && [ "$ASPNETCORE_ENVIRONMENT" != Production ]; then
  update-ca-certificates
fi

echo "Starting Backend For Web Frontend..."
dotnet BackendForWebFrontend.dll