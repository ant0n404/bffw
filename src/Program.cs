using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Duende.Bff.Yarp;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
});

builder.Services.AddControllers();

JwtSecurityTokenHandler.DefaultMapInboundClaims = false;

// WARNING: Use only for local development purposes!
// IdentityModelEventSource.ShowPII = true;

builder.Services
    .AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
        options.DefaultSignOutScheme = OpenIdConnectDefaults.AuthenticationScheme;
    })
    .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
    {
        options.Cookie.Name = "GIBZAPP-BFF-WEB";
        options.Cookie.SameSite = SameSiteMode.Strict;
    })
    .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
    {
        options.Authority = builder.Configuration.GetValue<string>("OpenIdConnectAuthority");
        options.ClientId = builder.Configuration.GetValue<string>("OpenIdConnectClientId");
        options.ClientSecret = builder.Configuration.GetValue<string>("OpenIdConnectClientSecret");

        options.CallbackPath = "/bffw/signin-oidc";
        options.SignedOutCallbackPath = "/bffw/signout-callback-oidc";

        options.ResponseType = OpenIdConnectResponseType.Code;
        options.ResponseMode = OpenIdConnectResponseMode.Query;
        options.RequireHttpsMetadata = false;

        options.GetClaimsFromUserInfoEndpoint = true;
        options.MapInboundClaims = false;
        options.SaveTokens = true;

        options.Scope.Clear();
        options.Scope.Add("offline_access");
        options.Scope.Add("openid");
        options.Scope.Add("profile");
        options.Scope.Add("email");

        options.ClaimActions.MapJsonKey(JwtClaimTypes.Role, ClaimTypes.Role);
        options.ClaimActions.MapJsonKey(JwtClaimTypes.GivenName, ClaimTypes.GivenName);
        options.ClaimActions.MapJsonKey(JwtClaimTypes.FamilyName, ClaimTypes.Surname);

        options.TokenValidationParameters = new()
        {
            NameClaimType = "name",
            RoleClaimType = "role",
        };
    });

builder.Services.AddAuthorization();

builder.Services.AddBff(options => options.ManagementBasePath = "/bffw")
                .AddServerSideSessions()
                .AddRemoteApis();

builder.Services.AddReverseProxy()
                .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"))
                .AddBffExtensions();

var app = builder.Build();

app.UseForwardedHeaders();

app.UseRouting();
app.UseAuthentication();

app.UseBff();

app.UseAuthorization();

app.MapBffManagementEndpoints();

app.MapBffReverseProxy();

app.UseEndpoints(endpoints =>
{
    endpoints.MapBffReverseProxy();
});

// The controllers are NOT mapped as bff api endpoints (.AsBffApiEndpoint).
// If the health controller would be mapped as bff api endpoint, the anti-forgery check
// would apply and every request is required to carry the X-CSRF header.
app.MapControllers();

app.Run();